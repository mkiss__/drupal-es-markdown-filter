<?php
/**
 * @file
 * es_markdown_filter.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function es_markdown_filter_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'use text format markdown'.
  $permissions['use text format markdown'] = array(
    'name' => 'use text format markdown',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'filter',
  );

  return $permissions;
}
